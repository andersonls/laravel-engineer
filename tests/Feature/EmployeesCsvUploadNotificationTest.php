<?php

namespace Tests\Feature;

use App\Models\User;
use App\Notifications\EmployeesCsvUploadNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class EmployeesCsvUploadNotificationTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        Notification::fake();
        $this->user = User::factory()->create();
    }

    public function test_it_generates_sucess_message_without_errors()
    {
        $this->user->notify(new EmployeesCsvUploadNotification([]));

        Notification::assertSentTo($this->user, EmployeesCsvUploadNotification::class, function ($notification, $channels) {
            $this->assertContains('mail', $channels);

            $mailNotification = $notification->toMail($this->user);
            $this->assertEquals('Upload dos colaboradores realizado com sucesso', $mailNotification->subject);
            $this->assertEquals('success', $mailNotification->level);

            $this->assertEquals('Excelente! Seus colaboradores foram importados com sucesso =)', $mailNotification->introLines[0]);

            return true;
        });
    }

    public function test_it_generates_error_message_with_errors()
    {
        $errorMessage = 'Something went wrong.';

        $this->user->notify(new EmployeesCsvUploadNotification(['error' => $errorMessage]));

        Notification::assertSentTo($this->user, EmployeesCsvUploadNotification::class, function ($notification, $channels) use ($errorMessage) {
            $this->assertContains('mail', $channels);

            $mailNotification = $notification->toMail($this->user);
            $this->assertEquals('Erro ao realizar o upload dos colaboradores.', $mailNotification->subject);
            $this->assertEquals('error', $mailNotification->level);

            $this->assertEquals('Houve um erro ao processar a planilha dos colaboradores =( Confira mais detalhes abaixo.', $mailNotification->introLines[0]);
            $this->assertEquals($errorMessage, $mailNotification->introLines[1]);

            return true;
        });
    }
}
