<?php

namespace Tests\Feature;

use App\Jobs\ProcessEmployeesCsvJob;
use App\Models\Employee;
use App\Models\User;
use App\Notifications\EmployeesCsvUploadNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class ProcessEmployeesCsvJobTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        Notification::fake();
        $this->user = User::factory()->create();
    }

    public function test_it_should_not_accept_upload_csv_with_invalid_header()
    {
        $csv = <<<CSV
            |namee            |email              |document    |city     |state |start_date |
            |Bob Wilson       |bob@paopaocafe.com |13001647000 |Salvador |BA    |2020-01-15 |
        CSV;

        ProcessEmployeesCsvJob::dispatch($csv, $this->user);

        Notification::assertSentTo($this->user, EmployeesCsvUploadNotification::class, function ($notification, $channels) {
            return ['error' => 'Invalid CSV header'] === $notification->data;
        });
    }

    public function test_it_uploads_csv_with_a_auth_user_and_inserts_records_into_database()
    {
        $this->assertEquals(Employee::count(), 0);

        $csv = <<<CSV
            |name             |email              |document    |city     |state |start_date |
            |Bob Wilson       |bob@paopaocafe.com |13001647000 |Salvador |BA    |2020-01-15 |
            |Laura Matsuda    |lm@matsuda.com.br  |60095284028 |Niterói  |RJ    |2019-06-08 |
            |Marco Rodrigues  |marco@kyokugen.org |71306511054 |Osasco   |SC    |2021-01-10 |
            |Christie Monteiro|monteiro@namco.com |28586454001 |Recife   |PE    |2015-11-03 |
        CSV;

        ProcessEmployeesCsvJob::dispatch($csv, $this->user);

        Notification::assertSentTo($this->user, EmployeesCsvUploadNotification::class, function ($notification, $channels) {
            return $notification->data === [];
        });

        $this->assertDatabaseHas('employees', [
            'user_id' => $this->user->id,
            'name' => 'Bob Wilson',
            'email' => 'bob@paopaocafe.com',
            'document' => '13001647000',
            'city' => 'Salvador',
            'state' => 'BA',
            'start_date' => '2020-01-15'
        ]);

        $this->assertEquals(Employee::count(), 4);
    }

    public function test_it_uploads_csv_with_a_auth_user_and_updates_existing_records_into_database()
    {
        Employee::create([
            'user_id' => $this->user->id,
            'name' => 'Marco Rodrigues',
            'email' => 'marco@kyokugen.org',
            'document' => '71306511054',
            'city' => 'Osasco',
            'state' => 'SC',
            'start_date' => '2021-01-10'
        ]);

        $csv = <<<CSV
            |name                        |email              |document    |city     |state |start_date |
            |Marco Rodrigues Dos Santos  |marco@convenia.com |71306511054 | Santos  |SP    |2021-01-10 |
        CSV;

        ProcessEmployeesCsvJob::dispatch($csv, $this->user);

        Notification::assertSentTo($this->user, EmployeesCsvUploadNotification::class, function ($notification, $channels) {
            return $notification->data === [];
        });

        $this->assertDatabaseHas('employees', [
            'user_id' => $this->user->id,
            'name' => 'Marco Rodrigues Dos Santos',
            'email' => 'marco@convenia.com',
            'document' => '71306511054',
            'city' => 'Santos',
            'state' => 'SP',
            'start_date' => '2021-01-10'
        ]);

        $this->assertEquals(Employee::count(), 1);
    }

    public function test_it_cannot_update_employee_from_another_user_and_creates_a_new_record_instead()
    {
        $user = User::factory()->create();

        $employee = Employee::create([
            'user_id' => $user->id,
            'name' => 'Marco Rodrigues',
            'email' => 'marco@kyokugen.org',
            'document' => '71306511054',
            'city' => 'Osasco',
            'state' => 'SC',
            'start_date' => '2021-01-10'
        ]);

        $csv = <<<CSV
            |name             |email              |document    |city     |state |start_date |
            |Marco Rodrigues  |marco@kyokugen.org |71306511054 | Osasco  |SC    |2021-01-10 |
        CSV;

        ProcessEmployeesCsvJob::dispatch($csv, $this->user);

        Notification::assertSentTo($this->user, EmployeesCsvUploadNotification::class, function ($notification, $channels) {
            return $notification->data === [];
        });

        $this->assertDatabaseHas('employees', [
            'user_id' => $user->id,
            'name' => $employee->name,
            'email' => $employee->email,
            'document' => $employee->document,
            'city' => $employee->city,
            'state' => $employee->state,
            'start_date' => $employee->start_date
        ]);

        $this->assertDatabaseHas('employees', [
            'user_id' => $this->user->id,
            'name' => $employee->name,
            'email' => $employee->email,
            'document' => $employee->document,
            'city' => $employee->city,
            'state' => $employee->state,
            'start_date' => $employee->start_date
        ]);

        $this->assertEquals(Employee::count(), 2);
    }

    public function test_it_cannot_upload_csv_with_invalid_data()
    {
        $this->assertEquals(Employee::count(), 0);

        $csv = <<<CSV
            |name             |email              |document    |city     |state |start_date |
            |Marco Rodrigues  |marco@kyokugen.org |71306511054 | Osasco  |SC    |2021-02-29 |
        CSV;

        ProcessEmployeesCsvJob::dispatch($csv, $this->user);

        Notification::assertSentTo($this->user, EmployeesCsvUploadNotification::class, function ($notification, $channels) {
            return ['error' => 'The given data was invalid.'] === $notification->data;
        });

        $this->assertEquals(Employee::count(), 0);
    }
}
