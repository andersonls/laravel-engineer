<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmployeesCsvUploadNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var array
     */
    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = new MailMessage();

        $message->greeting('Olá!');

        if (isset($this->data['error'])) {
            $message->error()
                    ->subject('Erro ao realizar o upload dos colaboradores.')
                    ->line('Houve um erro ao processar a planilha dos colaboradores =( Confira mais detalhes abaixo.')
                    ->line($this->data['error']);
        } else {
            $message->success()
                    ->subject('Upload dos colaboradores realizado com sucesso')
                    ->line('Excelente! Seus colaboradores foram importados com sucesso =)');
        }

        return $message;
    }
}
