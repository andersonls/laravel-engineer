<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    public const VALIDATION_RULES = [
        'user_id' => 'required|integer',
        'name' => 'required|string',
        'email' => 'required|email',
        'document' => 'required|string',
        'city' => 'required|string',
        'state' => 'required|size:2',
        'start_date' => 'required|date',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'document',
        'city',
        'state',
        'start_date',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
