<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Employee;
use App\Notifications\EmployeesCsvUploadNotification;
use InvalidArgumentException;

class ProcessEmployeesCsvJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $csv;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $csv, User $user)
    {
        $this->user = $user;
        $this->csv = $csv;
    }

    /**
     * The unique ID of the job.
     *
     * @return string
     */
    public function uniqueId()
    {
        return $this->user->id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $csv = explode(PHP_EOL, $this->csv);

        $header = str_getcsv(array_shift($csv), '|');
        $header = array_filter(array_map('trim', $header));

        $expectedHeader = ['name', 'email', 'document', 'city', 'state', 'start_date'];

        try {
            if (array_values($header) !== $expectedHeader) {
                throw new InvalidArgumentException('Invalid CSV header');
            }
            DB::transaction(function () use ($csv) {
                foreach ($csv as $line) {
                    $line = str_getcsv($line, '|');

                    $employeeData = [
                        'email' => trim($line[2]),
                        'name' => trim($line[1]),
                        'city' => trim($line[4]),
                        'state' => trim($line[5]),
                        'start_date' => trim($line[6]),
                        'user_id' => $this->user->id,
                        'document' => trim($line[3])
                    ];

                    Validator::make($employeeData, Employee::VALIDATION_RULES)->validate();

                    Employee::updateOrCreate([
                        'user_id' => $this->user->id,
                        'document' => $employeeData['document']
                    ], [
                        'email' => $employeeData['email'],
                        'name' => $employeeData['name'],
                        'city' => $employeeData['city'],
                        'state' => $employeeData['state'],
                        'start_date' => $employeeData['start_date']
                    ]);
                }
            });

            $this->user->notify(new EmployeesCsvUploadNotification([]));
        } catch (\Exception $e) {
            $this->user->notify(new EmployeesCsvUploadNotification(['error' => $e->getMessage()]));
        }
    }
}
