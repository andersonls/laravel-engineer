<?php

namespace App\Http\Controllers;

use App\Http\Requests\CsvUploadRequest;
use App\Jobs\ProcessEmployeesCsvJob;
use App\Models\Employee;
use Illuminate\Http\JsonResponse;

class EmployeeController extends Controller
{
    public function get(): JsonResponse
    {
        return response()->json(
            Employee::where('user_id', auth()->user()->id)->get()
        );
    }

    public function show(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json(
            $employee
        );
    }

    public function destroy(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json([
            'Success' => $employee->delete()
        ]);
    }

    public function uploadCsv(CsvUploadRequest $request): JsonResponse
    {
        $csv = $request->validated()['file']->getContent();

        ProcessEmployeesCsvJob::dispatch($csv, auth()->user());

        return response()->json([
            'Success' => true
        ]);
    }
}
